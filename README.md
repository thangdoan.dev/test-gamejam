# Gamejam Test


## Running
- Run the project with this command for the first time:

    ```bash
    $ sh runner.sh
    ```

- The second time: comment these lines
    ```bash
    # cp .env.template .env
    # python -m venv venv
    # source venv/Scripts/activate
    # pip install -r requirements.txt
    ```

## Testing
- With the `curl_import.txt` as added to this project. Import it into your postman or test it on your terminal/shell
