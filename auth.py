from functools import wraps
import os

from flask import jsonify, make_response, request

def basic_auth_required(f):
    @wraps(f)
    def main_func(**kwargs):
        auth = request.headers.get('Authorization')
        print(os.environ.get('BASIC_AUTH'))
        if not auth or (auth != f"Basic {os.getenv('BASIC_AUTH')}"):
            return make_response(jsonify({"message": "Invalid Username or Password"}), 401)
        return f(**kwargs)
    return main_func