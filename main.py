
from asyncio.log import logger
from datetime import datetime
import json
from select import select
import sqlite3
from flask import Flask, jsonify, make_response, request
from auth import basic_auth_required
from dotenv import load_dotenv
import logging

app = Flask(__name__)

logger = logging.getLogger()

sqlite_connection = sqlite3.connect('test.db', check_same_thread=False)
cursor = sqlite_connection.cursor()

# @app.route('/init-db')
# @basic_auth_required
def init_db():
    cursor.execute("""
        create table campaign (day date, campaign text, installs long, spent long)
    """)

    campaign_data = [
        ('2020-03-13', 'C1', 2400, 745),
        ('2020-03-14', 'C1', 2400, 745),
        ('2020-03-15', 'C1', 2400, 745),
        ('2020-03-16', 'C1', 2400, 745),
        ('2020-03-17', 'C1', 0, 0),
        ('2020-03-18', 'C1', 0, 0),
        ('2020-03-19', 'C1', 0, 0),
    ]

    cursor.executemany("""insert into campaign values (?, ?, ?, ?)""", campaign_data)

    cursor.execute("""
        create table revenue (spent_day date, revenue real, create_date date)
    """)

    revenue_data = [
        ('2020-03-13', 408.06, '2020-03-13'),
        ('2020-03-13', 83.09, '2020-03-14'),
        ('2020-03-13', 35.68, '2020-03-15'),
        ('2020-03-13', 26.87, '2020-03-16'),
        ('2020-03-14', 480.27, '2020-03-14'),
        ('2020-03-14', 98.78, '2020-03-15'),
        ('2020-03-14', 36.67, '2020-03-16'),
        ('2020-03-14', 0, '2020-03-17'),
        ('2020-03-15', 416, '2020-03-15'),
        ('2020-03-15', 85.29, '2020-03-16'),
        ('2020-03-15', 0, '2020-03-17'),
        ('2020-03-15', 0, '2020-03-18'),
        ('2020-03-16', 449.47, '2020-03-16'),
        ('2020-03-16', 0, '2020-03-17'),
        ('2020-03-16', 0, '2020-03-18'),
        ('2020-03-16', 0, '2020-03-19'),
    ]

    cursor.executemany("""insert into revenue values (?, ?, ?)""", revenue_data)

    sqlite_connection.commit()
    return make_response(jsonify({"message": "success"}))


@app.route('/revenue', methods=['GET'])
@basic_auth_required
def calculate_revenue():
    current_date = datetime.now().strftime('%Y-%m-%d')
    campaign_end_date = request.args.get('end_date', current_date)
    print(f'campaign_end_date: {campaign_end_date}')
    try:

        # Step 1: Sum the total spent in the campaign
        campaign_sql = f"""
            select sum(spent)
            from campaign 
            where day >= '2020-03-13' 
            and day <= '{campaign_end_date}'
            """
        cursor.execute(campaign_sql)
        total_amount = cursor.fetchall()[0][0]
        # total_amount = None

        # Step 2:  calculate total days can get back all the money invested

        revenue_sql = """
            select revenue
            from revenue
            where spent_day >= '{campaign_start_date}' 
            and spent_day <= '{campaign_end_date}'
        """

        cursor.execute(revenue_sql.format(campaign_start_date='2020-03-13',
        campaign_end_date=campaign_end_date))
        total_revenue = cursor.fetchall()
        print(total_revenue)
        final_revenue = calculate_total_revenue(total_revenue=total_revenue)
    
        cursor.execute(revenue_sql.format(campaign_start_date='2020-03-16',
        campaign_end_date=campaign_end_date))
        total_days, extra_profit = calculate_total_days(initial_investment=total_amount,
        total_revenue=cursor.fetchall())


        return make_response(jsonify({
            "message": "success", 
            "total_invested": total_amount,
            "predicted_days_to_retrieve_all_investment": total_days,
            "total_revenue": final_revenue,
            "extra_profit_predicted": abs(extra_profit)
        }))
    except Exception as ex:
        print(ex)
        return make_response(jsonify(
            {
                "message": "failed",
                "description": "you may enter wrong input format or our system having some errors"
            }
        ))


def calculate_total_days(initial_investment: int, total_revenue: list):
    sum_revenue = sum([value[0] for value in total_revenue])
    total_days = 0
    refunded_investment = initial_investment - sum_revenue
    if not refunded_investment:
        return total_days, 0.00
    average_amount = sum_revenue / len(total_revenue)
    while refunded_investment > 0:
        refunded_investment -= average_amount
        total_days += 1
    return total_days, round(refunded_investment, 2)


def calculate_total_revenue(total_revenue: list):
    total_revenue = sum([value[0] for value in total_revenue])
    return round(total_revenue, 2)

if __name__ == '__main__':
    load_dotenv()
    app.run('0.0.0.0', 5000)
    sqlite_connection.close()