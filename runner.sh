#!/bin/bash

cp .env.template .env
python -m venv venv
source venv/Scripts/activate
pip install -r requirements.txt

python main.py